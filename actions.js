window.onload = function() {
    var element = document.getElementsByClassName('element')[0];
    var offsetX = 0;
    var offsetY = 0;

    element.addEventListener('mousedown', () => {
        offsetX = event.offsetX;
        offsetY = event.offsetY;
        document.addEventListener('mousemove', mouseMoveHandler);
    });

    document.addEventListener('mouseup', () => {
        document.removeEventListener('mousemove', mouseMoveHandler);
    });

    function mouseMoveHandler(event) {
        element.style.left = event.pageX - offsetX + 'px';
        element.style.top = event.pageY - offsetY + 'px';
    }
}